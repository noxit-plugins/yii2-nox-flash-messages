Yii PHP Framework Version 2 / NOX Flash Messages
================================================

Yii2 NOX Flash Messages é uma biblioteca com métodos úteis para mensagens de sistema (gravadas na sessão) para o Framework Yii2.

![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)
